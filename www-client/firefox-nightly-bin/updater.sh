#!/bin/sh

set -e

export LANG=en_US.UTF-8
export TZ=UTC

TODAY=$(date +"%Y%m%d")

EBUILD_DIR=${0%/*}

cd "$EBUILD_DIR"

ebuild=$(echo *.ebuild)
ebuild_ver=$(echo "$ebuild" | sed -E 's/.*bin-([[:digit:]].+)\.ebuild$/\1/')

web_dir=$(sed -En 's/^MOZ_HTTP_URI="([^"]+)".*/\1/p' "$ebuild")

latest_release=$(\
    curl -s -L -o - "$web_dir" \
    | grep 'en-US\.linux-x86_64\.tar\.bz2"' \
    | sed -E 's/.*>([^<]+)<.*/\1/' \
    | sort -t - -k 2 -V -r \
    | head -1 \
    | sed -E \
        -e 's/^firefox-(.+).en-US.linux-x86_64.tar.bz2$/\1/' \
        -e 's/(\.[0-9]+)([^.0-9])/\1.'$TODAY'\2/' \
        -e 's/(\.[[:digit:]]+)a([[:digit:]]*)/\1_alpha\2/' \
)

which_latest=$(\
    for v in "$ebuild_ver" "$latest_release"; do echo "$v"; done \
    | sort -r -V | head -n 1 \
)

if [ "$which_latest" = "$ebuild_ver" ]; then
    echo "Already up to date."
    exit 0
fi

# Update ebuild.

new_ebuild=$(echo "$ebuild" | sed -E 's/(.*bin-).+(.ebuild)$/\1'"$latest_release"'\2/')

git mv "$ebuild" "$new_ebuild"

[ -f Manifest ] && rm Manifest

ebuild "$new_ebuild" manifest

git commit -a -m'bump firefox version'
git push
